# Question BOT

A [NodeJs](https://nodejs.org/en/) bot built in order to create channel in a guild.

## Features

- Create a channel in a category named 'Questions'
- Delete a channel (in the 'Questions' category only)

## Tech

This bot use:

- [discord.js](https://discord.js.org/#/)
- [NodeJs](https://nodejs.org/en/)
- [dotenv](https://www.npmjs.com/package/dotenv)

## How to use it

Start by forking the repositorie. Then add a .env 
```
TOKEN=<replace with the discord bot token>
EASTER1=<replace with a easter egg>
EASTER2=<replace with a easter egg>
EASTER3=<replace with a easter egg>
EASTER4=<replace with a easter egg>
EASTER5=<replace with a easter egg>
```


## Hosting

The easiest way to host your bot is via a VPS server, avaible for 1 to 5 dollars / months.
Notice that 500mb of ram, 1 cpu and 5gb of ssd is enough to make you bot avaible to everyone.

## TODO

- [ ] Clean code

