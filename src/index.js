const Discord = require("discord.js");
const client = new Discord.Client();
const dotenv = require("dotenv");
dotenv.config();

const DT = {
    parent_id: new Map()
}

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}`);
});

client.on("message", async (message) => {
  if (!client.application?.owner)
    client.application = await client.fetchApplication();

    if (message.content.toLocaleLowerCase().includes(process.env.EASTER1) || message.content.toLocaleLowerCase().includes(process.env.EASTER2)) {
        message.channel.send("I'm inevitable")
    } 
    if (message.content.toLocaleLowerCase() === process.env.EASTER3.toLocaleLowerCase()) {
        message.channel.send("https://tenor.com/view/thanos-infinity-war-everything-avengers-gif-14066494");
    }

    if (message.content.toLocaleLowerCase() === process.env.EASTER4.toLocaleLowerCase()) {
      message.channel.send("https://www.youtube.com/watch?v=PGNiXGX2nLU");
    }
    if (message.content.toLocaleLowerCase() === process.env.EASTER5.toLocaleLowerCase()) {
      message.channel.send("https://tenor.com/view/palmashow-bruant-bruantax-bruantinho-caffaffa-gif-20047396");
    }
  if (
    message.content.toLowerCase() === "!deploy" &&
    message.author.id === client.application?.owner.id
  ) {
    const commands = [
      {
        name: "question",
        description: "Create a channel with the name you give in parameter",
        options: [
          {
            name: "channel_name",
            description: "The name of the channel",
            type: 3,
            required: true,
          },
        ],
      },
      {
        name: "delete",
        description: "delete the channel you give in parameter",
        options: [
          {
            name: "channel",
            description: "The channel to delete",
            type: 7,
            required: true,
          },
        ],
      },
    ];

    const current_commmand = await client.api
        .applications(client.user.id)
        .guilds(message.guild.id)
        .commands.get();
    
    await Promise.all(current_commmand.map(async (c) => client.api
    .applications(client.user.id)
    .guilds(message.guild.id)
    .commands(c.id).delete()));

    commands.forEach(async (command) => {
      const c = await client.api
        .applications(client.user.id)
        .guilds(message.guild.id)
        .commands.post({ data: command });
    });

    createCategorieChannel(message.guild.id);
  }
});

function getParameterByName(arr, str) {
  return arr.find((el) => el.name === str);
}

async function createChan(guild, str) {
    await createCategorieChannel(guild.id)
  await guild.channels.create(str, {parent: DT.parent_id.get(guild.id)});
}

async function respondQuestion(interaction) {
  const guild = await client.guilds.fetch(interaction.guild_id);
  await createChan(
    guild,
    getParameterByName(interaction.data.options, "channel_name").value
  );

  return {
    data: {
      type: 4,
      data: {
        content: `Try create a new channel: ${
          getParameterByName(interaction.data.options, "channel_name").value
        }`,
      },
    },
  };
}

async function getCategoriesChannel() {
    return client.channels.cache.filter(el => el.type === "category");
}

async function categorieChannelExist(guild_id) {
    const ct = getCategoriesChannel();
    return (await ct).find(el => el.name === "Questions" && el.guild.id === guild_id);
} 

async function createCategorieChannel(guild_id) {
    const ctc = await categorieChannelExist(guild_id);
    if (!ctc || ctc.deleted) {
        const guild = await client.guilds.fetch(guild_id);
        const c = await guild.channels.create("Questions", {type: "category"});
        DT.parent_id.set(guild_id, c.id);
        return;
    }
    DT.parent_id.set(guild_id, ctc.id);
}


async function respondDelete(interaction) {
    await createCategorieChannel(interaction.guild_id)
    const channel = await client.channels.fetch(getParameterByName(interaction.data.options, "channel").value);
    if (!channel.parentID) {
        return {
            data: {
                type: 4,
                data : {
                    content : `I WILL NOT DESTROY ORPHANS`
                }
            }
        }
    }
    if (channel.parentID !== DT.parent_id.get(interaction.guild_id)) {
        return {
            data: {
                type: 4,
                data : {
                    content : `I ONLY DESTROY CHILD OF MINE: QUESTION`
                }
            }
        }
    }
    await channel.delete();
    return {
        data: {
            type: 4,
            data : {
                content : `I JUST HAVE DESTROY MY CHILD ${channel.name}`
            }
        }
    }
}

client.ws.on("INTERACTION_CREATE", async (interaction) => {
  if (interaction.type === 1) {
    client.api.interactions(interaction.id, interaction.token).callback.post({
      data: {
        type: 1,
      },
    });
    return;
  }
  if (interaction.data.name === "question") {
    const rep = await respondQuestion(interaction);
    client.api
      .interactions(interaction.id, interaction.token)
      .callback.post(rep);
    return;
  }

  if (interaction.data.name === "delete") {
    const rep = await respondDelete(interaction);
    client.api
      .interactions(interaction.id, interaction.token)
      .callback.post(rep); 
    return;
  }
});

client.login(process.env.TOKEN);
